CREATE DATABASE  IF NOT EXISTS `recipe` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `recipe`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: recipe
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(150) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0 = Inactive, 1 = Active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Appetizers',1,'2017-10-18 12:09:01','2017-10-18 04:09:01'),(2,'Soup',1,'2017-10-18 12:09:01','2017-10-18 04:09:01'),(3,'Main Dish',1,'2017-10-18 12:09:01','2017-10-18 04:09:01'),(4,'Dessert',1,'2017-10-18 12:09:01','2017-10-18 04:09:01');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL,
  `ingredients` varchar(3000) DEFAULT NULL,
  `directions` text,
  `preparation_time` varchar(45) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0 = Deleted, 1 = Active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `featured` int(1) NOT NULL DEFAULT '0' COMMENT '0 = None featured, 1 = Featured',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,4,'Creme Brulee','uploads/dcaab2fc7680c2190eb100d895b18ced.jpg','ingredients','directions','1 hour',1,'2017-10-18 13:36:53','2017-10-18 06:04:17',0),(2,3,'Black Angus Burger','','1 pound Certified Angus Beef ® ground chuck\r\n4 crusty burger buns, sliced in half\r\n4 leaves green leaf lettuce\r\n1 tomato, sliced\r\n4 slices red onion, grilled\r\n4 ounces crumbled blue cheese\r\n4 tablespoons whole grain mustard\r\nSalt and blackening spice to taste','Instructions:\r\n\r\n1. Preheat grill. Portion chuck into four patties. Season burgers with salt and blackening spice, grill to an internal temperature of 160°F.\r\n2. Build sandwich by layering the lettuce, tomato, burger, onion and blue cheese. Spread the top of the bun with mustard.','20 min',1,'2017-10-18 14:09:30','2017-10-18 06:09:30',0),(3,1,'Grilled Oyster with Cheese','uploads/96c9cde2e793924264f7066c10c0e189.jpeg','Cheese Topping:\r\n2 cups grated Parmesan cheese\r\n1 cup grated mozzarella cheese \r\n2 tablespoons granulated garlic\r\n2 lemons, zested and juiced\r\n1 bunch Italian parsley, chopped \r\nGarlic Finishing Butter:\r\n1 stick unsalted butter\r\n1 cup fresh lemon juice \r\n1/2 cup hot sauce, such as the Louisiana brand\r\n1/2 cup Worcestershire sauce \r\n1/4 cup chopped garlic \r\n2 tablespoons granulated garlic\r\n24 oysters, shucked on the half shell\r\n1 loaf Italian bread, sliced and charred on the grill','For the cheese topping: Mix the Parmesan, mozzarella, garlic, lemon juice, lemon zest and parsley in a bowl and reserve for later use.\r\n\r\nFor the finishing butter: Place the butter, lemon juice, hot sauce, Worcestershire, chopped garlic, granulated garlic and 1/2 cup water into a small pot on the stove. Slowly melt and whisk ingredients until incorporated.\r\n\r\nFor the oysters: Preheat the grill until it is very hot, about 400 degrees F.\r\n\r\nTop each oyster with a liberal amount of the cheese mixture. Carefully place the oysters on the grill and close. Cook until the cheese is melted and the oysters are starting to get some color, about 10 minutes.\r\n\r\nRemove the oysters from the grill with tongs and place them onto a cookie sheet or a heat-proof surface.\r\n\r\nArrange 6 oysters per plate or leave on the large platter. Ladle some melted garlic butter over the oysters.\r\n\r\nServe with grilled bread to soak up the juices and butter!','30 minutes',1,'2017-10-18 14:13:52','2017-10-18 06:13:52',0),(4,2,'Chicken Soup','uploads/76546ce7a6031b0ed14f96fb65c2cba5.jpg','1 tablespoon butter\r\n1/2 cup chopped onion\r\n1/2 cup chopped celery\r\n4 (14.5 ounce) cans chicken broth\r\n1 (14.5 ounce) can vegetable broth','1. Put the chicken, carrots, celery and onion in a large soup pot and cover with cold water. Heat and simmer, uncovered, until the chicken meat falls off of the bones (skim off foam every so often).\r\n\r\n2. Take everything out of the pot. Strain the broth. Pick the meat off of the bones and chop the carrots, celery and onion. Season the broth with salt, pepper and chicken bouillon to taste, if desired. Return the chicken, carrots, celery and onion to the pot, stir together, and serve.','1 hour',1,'2017-10-19 10:48:53','2017-10-19 06:49:55',1);
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-20 10:55:05
