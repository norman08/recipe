<?php 
/**
 * formatURL
 */
function formatURL($url = NULL)
{
    $result = $url;

    if(!empty($url)) {
        $result = str_replace("_", "-", strtolower($url));
    }

    return $result;
}

/*
 | ------------------------------------------------------------------
 |  Encryption Library
 | ------------------------------------------------------------------
 |
 |  SAMPLE: encryptLib("encode", 1234)
 |  
 | @param STRING $action
 | @param STRING $string
 | @return string $result
 */

function encryptLib($string = NULL, $action = "encode") {
    $CI     =& get_instance();
    $result = "";
    $key    = generateRandomCode();
    if(strtoupper($action) == "ENCODE") {
        $key = $key . "_" . $string . "_" . date("mYdiHs"); 
        $result = base64_encode ( $key );
    } else {
        $decode = base64_decode($string);
        $xpl    = explode("_", $decode);

        $result = @$xpl[1];
    }

    return $result;
}

/**
 * errorMessage
 * @param   string      $action         //error, success
 * @param   string      $data           // error message
 *
 * @return  string      $div            //True or false
 */
function errorMessage($action = 'error', $data = NULL)
{
    if($action == 'error') {
        $div = '<div class="alert alert-danger alert-dismissible">';
        $div .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        $div .= '<h4><i class="icon fa fa-ban"></i> Alert!</h4>';
        $div .= $data;
        $div .= '</div>';
    } else {
        $div = '<div class="alert alert-success alert-dismissible">';
        $div .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        $div .= '<h4><i class="icon fa fa-check"></i> Success!</h4>';
        $div .= $data;
        $div .= '</div>';
    }

    return $div;
}

/**
 * _paginationConfig
 * @param   string      $url                // url 
 * @param   string      $totalRecords       // total records
 * @param   string      $limitPerPage       // limit per page
 * @param   string      $uriSegment         // uri segment
 *
 * @return  array      $config              //array
 */
function _paginationConfig($url, $totalRecords, $limitPerPage, $uriSegment = 3)
{
    $config['base_url']     = $url;
    $config['total_rows']   = $totalRecords;
    $config['per_page']     = $limitPerPage;
    $config["uri_segment"]  = $uriSegment;
     
    // custom paging configuration
    $config['num_links']                = 2;
    $config['use_page_numbers']         = TRUE;
    $config['reuse_query_string']       = TRUE;   
    

    $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
    $config['full_tag_close']   = '</ul></nav></div>';
    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
    $config['num_tag_close']    = '</span></li>';
    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['prev_tagl_close']  = '</span></li>';
    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
    $config['first_tagl_close'] = '</span></li>';
    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
    $config['last_tagl_close']  = '</span></li>';

    return $config;
}