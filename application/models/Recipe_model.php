<?php
class Recipe_model extends CI_Model 
{
    public $table = 'recipe';
    
    public function __construct() 
    {
        parent::__construct();
    }    

    public function add($data)
    {
        $query = $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function update($where, $data)
    {
        $this->db->where($where);
        $sql = $this->db->update($this->table, $data);
        return true;
    }

    public function get_where( $where = NULL, $sort = null )
    {
        $this->db->select('*');
        $this->db->from($this->table);

        if(!empty($where)){
            $this->db->where( $where );
        }

        if(!empty($sort)){
            $this->db->order_by($sort);
        }
        
        $result = $this->db->get();     
        $res_qry = $result->result_array();
        return $res_qry;
    }

    public function delete($where)
    {
        $this->db->where( $where );
        $this->db->delete($this->table); 
        return true;
    }

    public function getJoinWhere( $where = NULL, $sort = null, $limit = null)
    {
        $this->db->select('r.*, c.description');
        $this->db->from($this->table . '  r');
        $this->db->join('categories c', 'c.id = r.category_id', 'left');

        if(!empty($where)){
            $this->db->where( $where );
        }

        if(!empty($sort)){
            $this->db->order_by($sort);
        }

        if(!empty($limit)){
            $this->db->limit($limit);
        }
        
        $result = $this->db->get();
        $res_qry = $result->result_array();
        return $res_qry;
    }

    public function getJoinPageWhere( $where = NULL, $sort = null, $limit = 0, $start = 0)
    {
        $this->db->select('r.*, c.description');
        $this->db->from($this->table . '  r');
        $this->db->join('categories c', 'c.id = r.category_id', 'left');

        if(!empty($where)){
            $this->db->where( $where );
        }

        if(!empty($sort)){
            $this->db->order_by($sort);
        }

        // if(!empty($limit) && ){
        $this->db->limit($limit, $start);
        //}
        
        $result = $this->db->get();     
        $res_qry = $result->result_array();
        return $res_qry;
    }

    public function getTotal($where) 
    {
        $query = $this->db->query("SELECT COUNT(id) AS totalCount FROM recipe WHERE status = 1 $where");
        return $query->row_array();
    }
}
/* End of file Recipe_model.php */
/* Location: ./system/application/models/Recipe_model.php */
