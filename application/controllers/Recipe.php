<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recipe extends MY_Controller {
    
    public $page_title  = 'Recipe';
    public $module_name = 'recipe';
    
    public function add()
    {
        $this->load->model("Categories_model");
        $data = array();
        $data['categories'] = $this->Categories_model->get_where(array('status' => 1), 'description asc');

        $this->load_main_html($this->module_name.'/add', $data);
    }

    public function process()
    {
        $this->load->model("Recipe_model");

        $category           = $this->input->post("category");
        $name               = $this->input->post("name");
        // $ingredients        = $this->input->post("ingredients");
        $ingredients        = $this->input->post("ingredients");
        $directions         = $this->input->post("directions");
        $preparation_time   = $this->input->post("preparation_time");

        $featured = 0;
        if($this->input->post('featured')) {
            $featured = 1;
        }

        //Upload configuration
        $imageName = md5(uniqid('', true));
        $config['upload_path']  = './uploads/';
        $config['allowed_types']= 'gif|jpg|png|jpeg';
        $config['max_size']     = 100;
        $config['max_width']    = 1024;
        $config['max_height']   = 768;
        $config['file_name']    = $imageName;

        $path = '';
        if($_FILES['userfile']['size'] > 0) {

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('userfile'))
            {
                // $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error_message', $this->upload->display_errors());
                //Redirect to add page
                redirect(base_url(formatURL($this->module_name). "/add"));

            } else {

                $filePath = 'uploads/'.$this->upload->data('file_name');
                $path = $filePath;
            }
        }

        $data = array(
                    'category_id'   => $category,
                    'name'          => $name,
                    'image'         => $path,
                    'ingredients'   => $ingredients,
                    'directions'    => $directions,
                    'preparation_time' => $preparation_time,
                    'status'        => 1,
                    'featured'      => $featured,
                    'created_at'    => date("Y-m-d H:i:s"),
                    'updated_at'    => date("Y-m-d H:i:s")
                );

        $resAdd = $this->Recipe_model->add($data);

        if(is_numeric($resAdd)) {
            $this->session->set_flashdata('success_message', 'Successfully saved.');
            // $encrpytedID = encryptLib($resAdd);
            redirect(base_url(formatURL($this->module_name). "/edit/$resAdd"));
        } else {
            $this->session->set_flashdata('error_message', 'Failed to add recipe.');
            redirect(base_url(formatURL($this->module_name). "/add"));
        }
    }

    public function edit($id)
    {
        $this->load->model("Categories_model");
        $this->load->model("Recipe_model");

        if(empty($id)) {
            $this->session->set_flashdata('error_message', 'Invalid request.');
            redirect(base_url(formatURL($this->module_name). "/add"));
        }

        $data = array();
        $data['categories'] = $this->Categories_model->get_where(array('status' => 1), 'description asc');

        $id = addslashes($id);
        $resRecipe = $this->Recipe_model->get_where(array('id' => $id));
        
        if(empty($resRecipe)) {
            $this->session->set_flashdata('error_message', 'Invalid request.');
            redirect(base_url(formatURL($this->module_name). "/add"));
        }

        $data['recipe'] = $resRecipe;
        $this->load_main_html($this->module_name.'/edit', $data);
    }

    public function update()
    {
        $this->load->model("Recipe_model");

        $recipeID           = $this->input->post("recipeID");
        $category           = $this->input->post("category");
        $name               = $this->input->post("name");
        // $ingredients        = $this->input->post("ingredients");
        $ingredients        = $this->input->post("ingredients");
        $directions         = $this->input->post("directions");
        $preparation_time   = $this->input->post("preparation_time");

        $featured = 0;
        if($this->input->post('featured')) {
            $featured = 1;
        }
        //Upload configuration
        $imageName = md5(uniqid('', true));
        $config['upload_path']  = './uploads/';
        $config['allowed_types']= 'gif|jpg|png|jpeg';
        $config['max_size']     = 100;
        $config['max_width']    = 1024;
        $config['max_height']   = 768;
        $config['file_name']    = $imageName;

        $path = '';
        if($_FILES['userfile']['size'] > 0) {

            $this->load->library('upload', $config);

            if(!$this->upload->do_upload('userfile'))
            {
                // $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error_message', $this->upload->display_errors());
                //Redirect to add page
                redirect(base_url(formatURL($this->module_name). "/add"));

            } else {

                $filePath = 'uploads/'.$this->upload->data('file_name');
                $path = $filePath;
            }
        }

        $data = array(
                    'category_id'   => $category,
                    'name'          => $name,                    
                    'ingredients'   => $ingredients,
                    'directions'    => $directions,
                    'preparation_time' => $preparation_time,
                    'status'        => 1,
                    'featured'      => $featured,
                    'updated_at'    => date("Y-m-d H:i:s")
                );

        if(!empty($path)) {
            $data = array_merge($data, array('image' => $path));
        }

        $where = array('id' => $recipeID);

        $resAdd = $this->Recipe_model->update($where, $data);

        if($resAdd) {
            $this->session->set_flashdata('success_message', 'Successfully saved changes.');
            // $encrpytedID = encryptLib($resAdd);
        } else {
            $this->session->set_flashdata('error_message', 'Failed to update recipe.');
        }
        
        redirect(base_url(formatURL($this->module_name). "/edit/$recipeID"));
    }

    public function view($name = NULL, $id = NULL)
    {
        $this->load->model("Recipe_model");

        $data   = array();
        $id     = addslashes($id);
        // $resCheck = $this->Recipe_model->get_where();
        $resCheck = $this->Recipe_model->getJoinWhere(array('r.id' => $id, 'r.status' => 1));
        if(empty($resCheck)) {
            redirect(base_url(formatURL('home')));
        }
        $data['recipe'] = $resCheck;
        $this->load_main_html($this->module_name.'/view', $data);
    }

    public function deleteRecord()
    {
        $this->load->model("Recipe_model");

        $id = $this->input->post("id");
        $id = addslashes($id);

        //Validate id if valid
        $resCheck = $this->Recipe_model->get_where(array('id' => $id, 'status' => 1));

        if(empty($resCheck)){
            echo json_encode(array('code' => 1, 'msg' => 'Failed. Invalid request.'));
            return;
        }

        $resDelete = $this->Recipe_model->delete(array('id' => $id));

        if($resDelete){
            echo json_encode(array('code' => 0, 'msg' => 'Successfully deleted'));
            return;
        } else {
            echo json_encode(array('code' => 1, 'msg' => 'Internal error occured. Please try again.'));
            return;
        }
    }

    public function category($category)
    {
        $this->load->model("Recipe_model");
        $this->load->model("Categories_model");
        $this->load->library('pagination');

        $category = addslashes($category);
        $category = str_replace("-", " ", $category);
        $resCategory = $this->Categories_model->get_where(array('description' => $category));
        // debug($resCategory,1);
        if(empty($resCategory)){
            redirect(base_url(formatURL('home')));   
        }

        $data = array();
        
        $limit = 2;
        
        $url = base_url() . 'recipe/category/'. str_replace(" ", "-", $category) . "/";

        $page = ($this->uri->segment(4)) ? ($this->uri->segment(4) - 1) : 0;
        // debug($this->uri->segment(4),1);
        // $page = ($page < 1) ? 0 : $page;

        $whereTotal = " AND category_id = " . $resCategory[0]['id'];
        $restTotal = $this->Recipe_model->getTotal($whereTotal);
        $totalRecords = $restTotal['totalCount'];

        $where = array('r.status' => 1, 'r.category_id' => $resCategory[0]['id'] );
        $sort  = 'r.created_at DESC';
        $start = $page*$limit;

        $data['recipe'] = $this->Recipe_model->getJoinPageWhere($where, $sort, $limit, $start);
        // debug($data,1);
        $uriSegment = 4;
        $config = _paginationConfig($url, $totalRecords, $limit, $uriSegment);

        $this->pagination->initialize($config);
        
        $data["links"] = $this->pagination->create_links();

        $this->load_main_html($this->module_name.'/category', $data);
    }
}

/* End of file Home.php */
/* Location: ./system/application/controllers/Home.php */
