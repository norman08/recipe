<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
    
    public $page_title  = 'Home';
    public $module_name = 'home';
    public function index()
    {
        $this->load->model("Recipe_model");

    	$data = array();

        $where = array('r.status' => 1);
        $sort  = 'r.created_at DESC';
        $limit = 3;
        $data['recipe'] = $this->Recipe_model->getJoinWhere($where, $sort, $limit);
        // debug($data,1);
    	$this->load_main_html($this->module_name.'/index', $data);
    }

    public function featured($debug = 0)
    {
        $this->load->model("Recipe_model");

        $where = array('r.status' => 1, 'r.featured' => 1);
        $sort  = 'r.created_at DESC';
        $limit = 1;
        $resFeatured = $this->Recipe_model->getJoinWhere($where, $sort, $limit);
        $data['featured'] = $resFeatured;
        if($debug == 1) {
            $this->load_main_html($this->module_name.'/featured', $data);
        } else {
            $resView = $this->load->view($this->module_name.'/featured', $data, true);
            echo $resView;
        }
    }
}

/* End of file Home.php */
/* Location: ./system/application/controllers/Home.php */
