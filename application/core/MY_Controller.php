<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{   
    /**
     * This variable holds the module name
     * @var string
     * @access public
     */
    public $module_name = NULL;
    /**
     * This variable holds the model name
     * @var string
     * @access public
     */
    public $model_name = NULL;

    /**
     * This variable holds the module name
     * @var string
     * @access public
     */
    public $page_title = NULL;

    /**
     * This variable holds the module name
     * @var string
     * @access public
     */
    public $menu_active = NULL;

     /**
     * This variable holds the module code
     * @var string
     * @access public
     */
    public $menu_code = NULL;

    /**
    * This variable holds the sub menu
    * @var string
    * @access public
    */
    public $sub_menu = NULL;
    
    
    function __construct() 
    {    
        parent::__construct();
    }
  
    public function load_main_html($url, $html_data=null)
    {
        $view_data = array();

        $data['html_header']    = $this->load->view('home_template/header', $view_data, true);
        $data['html_menu']      = $this->load->view('home_template/menu', $view_data, true);
        $data['html_body']      = $this->load->view($url, $html_data, true);
        $data['html_footer']    = $this->load->view('home_template/footer', null, true);        
        $this->load->view('home_template/index',$data);
    }
}