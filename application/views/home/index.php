<section>
    <div class="container">
        <div class="row">
            <div class="col-8">
                <?php foreach($recipe as $row) { ?>
                <div class="row recipe">
                    <div class="col-2">
                        <img src="<?php echo !empty($row['image']) ? base_url($row['image']) : base_url('uploads/no-photo.png')?>" style="width: 100%;">
                    </div>
                    <div class="col-10">
                        <h3><?php echo $row['name'] ?></h3>
                        <div>
                            <a href="<?php echo base_url("recipe/edit/".$row['id']) ?>"> <i class="fa fa-pencil"></i> Edit </a>
                            <a onclick="deleteRecord('<?php echo $row['id'] ?>')"> <i class="fa fa-times"></i> Delete </a>
                        </div>
                        <div><?php echo $row['description'] ?></div>
                        <label class="ingredients">Ingredients</label>
                        <div>
                            <?php 
                                
                                echo substr($row['ingredients'], 0,250);

                                if(strlen($row['ingredients']) > 250) {
                                    echo '......';
                                }

                                $recipeName = str_replace(" ", "-", $row['name']);
                            ?>                            
                        </div>
                        <div>
                            <a href="<?php echo base_url("recipe/view/$recipeName/".$row['id']) ?>">Read more</a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="col-4 text-center">
                <a href="<?php echo base_url("recipe/add") ?>" class="btn btn-primary">Submit A Recipe</a>
                <div class="col-12">
                    <div class="featured">
                        <div class="featured-header">Featured Recipe</div>
                        <div class="featured_data">
                            <!-- Featured data should appear here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>