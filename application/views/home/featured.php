<?php foreach ($featured as $row) { 
    $recipeName = str_replace(" ", "-", $row['name']);
    ?>
    <a href="<?php echo base_url("recipe/view/$recipeName/".$row['id']) ?>">
        <div class="col-11" style="margin-top: 10px;">
            <img src="<?php echo !empty($row['image']) ? base_url($row['image']) : base_url('uploads/no-photo.png')?>" style="width: 100%;">  
        </div>
        <h3><?php echo $row['name'] ?></h3>
    </a>
<?php } ?>