<section>
    <div class="container">
        <div class="row">
            <div class="col-8">
                <?php foreach($recipe as $row) { ?>
                <div class="row recipe">
                    <div class="col-2">
                        <img src="<?php echo !empty($row['image']) ? base_url($row['image']) : base_url('uploads/no-photo.png')?>" style="width: 100%;">
                    </div>
                    <div class="col-10">
                        <h3><?php echo $row['name'] ?></h3>
                        <div>
                            <a href="<?php echo base_url("recipe/edit/".$row['id']) ?>"> <i class="fa fa-pencil"></i> Edit </a>
                            <a onclick="deleteRecord('<?php echo $row['id'] ?>')"> <i class="fa fa-times"></i> Delete </a>
                        </div>
                        <div><?php echo $row['description'] ?></div>
                        <label class="ingredients">Ingredients</label>
                        <div>
                            <?php echo $row['ingredients']; ?>
                        </div>
                        <label class="ingredients">Directions</label>
                        <div>
                            <?php echo $row['directions']; ?>
                        </div>
                        <label class="ingredients">Preparation time</label>
                        <div>
                            <?php echo $row['preparation_time']; ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>