<section id="contact" class="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="form">
                    <?php 
                        if($this->session->flashdata('success_message')){
                            $data = $this->session->flashdata('success_message');
                            echo errorMessage('success', $data);
                        } else if($this->session->flashdata('error_message')) {
                            $data = $this->session->flashdata('error_message');
                            echo errorMessage('error', $data);
                        }
                    ?>
                    <form action="<?php echo base_url(formatURL($this->module_name) . "/update") ?>" method="post" id="addForm" class="contactForm" enctype="multipart/form-data">
                        <input type="hidden" id="recipeID" name="recipeID" value="<?php echo $recipe[0]['id'] ?>">
                        <div class="form-group">
                            <select class="form-control" name="category" id="category">
                                <?php foreach($categories as $row) { ?>
                                <option value="<?php echo $row['id'] ?>" <?php echo ($row['id'] == $recipe[0]['category_id']) ? 'selected' : '' ?>><?php echo $row['description'] ?></option>
                                <?php } ?>
                            </select>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name" data-msg="Please enter name" value="<?php echo $recipe[0]['name'] ?>"/>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <input type="file" class="form-control" name="userfile" id="userfile" placeholder="File" data-msg="Please upload a file" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="ingredients" id="ingredients" rows="5" data-rule="required" data-msg="Please add ingredients" placeholder="Ingredients"><?php echo $recipe[0]['ingredients'] ?></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="directions" id="directions" rows="5" data-rule="required" data-msg="Please add directions" placeholder="Directions"><?php echo $recipe[0]['directions'] ?></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="preparation_time" id="preparation_time" placeholder="Preparation time" value="<?php echo $recipe[0]['preparation_time'] ?>" data-msg="Please enter preparation time" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <label for="featured">
                                <input type="checkbox" name="featured" id="featured" <?php echo ($recipe[0]['featured']) ? 'checked' : '' ?>/>
                                Featured
                            </label>
                        </div>
                        <div class="text-center"><button type="submit">Save Changes</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        var errorClass = 'inline';
        var errorElement = 'em';
        
        var $checkoutForm = $('#addForm').validate({
            errorClass      : errorClass,
            errorElement    : errorElement,
            highlight: function(element) {
                $(element).parent().removeClass('has-success').addClass("has-error");
                $(element).removeClass('valid');
            },
            unhighlight: function(element) {
                $(element).parent().removeClass("has-error").addClass('has-success');
                $(element).addClass('valid');
            },

            // Rules for form validation
            rules : {
                category : {
                    required : true,
                },
                name : {
                    required : true,
                    minlength : 3
                },
                ingredients : {
                    required : true,
                    minlength : 3
                },
                directions : {
                    required : true,
                    minlength : 3
                },
                preparation_time : {
                    required : true
                },
            },
            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });        
    }); 
</script>