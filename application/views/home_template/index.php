<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Recipe | <?php echo $this->page_title; ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Recipe" name="keywords">
    <meta content="Recipe" name="description">
    
    <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
    <meta property="og:title" content="Recipe">
    <meta property="og:image" content="Recipe">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    
    <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <?php echo $html_header; ?>
    <body>
        <!-- BODY  -->
        <?php echo $html_menu; ?>
        <?php echo $html_body; ?>
        <?php echo $html_footer; ?>
    </body>
</html>