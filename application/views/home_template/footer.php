        <footer class="site-foot">
            <div class="bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-xs-12 text-lg-center text-center">
                            <hr>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                  <a href="<?php echo base_url('home') ?>">
                                    <!-- <i class="fa fa-facebook-square "></i> -->
                                        Home
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="<?php echo base_url('recipe/category/appetizers') ?>">
                                        Appetizers
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="<?php echo base_url('recipe/category/soup') ?>">
                                        Soup
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="<?php echo base_url('recipe/category/main-dish') ?>">
                                        Main Dish
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="<?php echo base_url('recipe/category/dessert') ?>">
                                        Dessert
                                    </a>
                                </li>

                            </ul>
                        </div>
                        <div class="col-lg-12 col-xs-12 text-lg-center text-center">
                            <p class="credits">           
                                cr8v Recipies © 2011 All Rights Reserved
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".featured_data").load('<?php echo base_url("home/featured") ?>');
            })

            function deleteRecord(id)
            {
                if(confirm('Are you sure you want to delete this recipe?')) {
                    $.ajax(
                    {
                        type    : "POST",
                        url     : '<?php echo base_url("recipe/deleteRecord"); ?>',
                        //url     : 'routes.php',
                        timeout : (300*1000),
                        data    : {
                            id      : id,
                        },
                        success : function(res_data)
                        {
                            var json = $.parseJSON(res_data);

                            if(json['code'] == 0) {
                                
                                setTimeout(function()
                                {
                                    window.location = "";
                                },1000); 

                            } else {
                                alert(json['msg']);
                            }
                        },
                        error:function(objAJAXRequest, strError)
                        {
                            alert('Internal error. Please contact administrator.');
                        }
                    });
                }
            }
        </script>
        <a class="scrolltop" href="#"><span class="fa fa-angle-up"></span></a> <!-- JavaScript-->
        <!-- Required JavaScript Libraries -->
        <script src="<?php echo base_url() ?>/assets/lib/superfish/hoverIntent.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/superfish/superfish.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/tether/js/tether.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/stellar/stellar.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/counterup/counterup.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/waypoints/waypoints.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/easing/easing.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/stickyjs/sticky.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/parallax/parallax.js"></script>
        <script src="<?php echo base_url() ?>/assets/lib/lockfixed/lockfixed.min.js"></script>    
        <!-- Template Specisifc Custom Javascript File -->
        <script src="<?php echo base_url() ?>/assets/js/custom.js"></script>    
        <!-- Summer Note -->
        <!-- <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script> -->

        <!-- <script src="<?php echo base_url() ?>/assets/contactform/contactform.js"></script> -->
    </body>
</html>