        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">
        
        <!-- Bootstrap CSS File -->
        <link href="<?php echo base_url() ?>/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      
        <!-- Libraries CSS Files -->
        <link href="<?php echo base_url() ?>/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        
        <!-- Main Stylesheet File -->
        <link href="<?php echo base_url() ?>/assets/css/animate.min.css" rel="stylesheet"> 
        <link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet">
        <script src="<?php echo base_url() ?>/assets/lib/jquery/jquery.min.js"></script>
    </head>