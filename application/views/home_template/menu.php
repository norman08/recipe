<div class="text-left header">
    <a class="hero-brand" href="<?php echo base_url() ?>" title="Home">
        <img class="animated zoomIn" style="width: 100px; height: 86px;" alt="Goldcoin Logo" src="<?php echo base_url() ?>/assets/img/rsz_recipe-logo.jpg">
    </a>
</div>
<!-- /Hero -->
<header id="header">
    <div class="container">
        <div id="logo" class="pull-left">
            <!-- Put your logo here -->
        </div>
        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="<?php echo strtolower($this->module_name) == 'home' ? 'menu-active' : '' ?>"><a href="<?php echo base_url("home") ?>">Home</a></li>
                <li class="<?php echo (strtolower($this->module_name) == 'recipe' && strtolower($this->uri->segment(3)) == 'appetizers' ) ? 'menu-active' : '' ?>">
                    <a href="<?php echo base_url("recipe/category/appetizers") ?>">
                        Appetizers
                    </a>
                </li>
                <li class="<?php echo (strtolower($this->module_name) == 'recipe' && strtolower($this->uri->segment(3)) == 'soup' ) ? 'menu-active' : '' ?>">
                    <a href="<?php echo base_url("recipe/category/soup") ?>">
                        Soup
                    </a>
                </li>
                <li class="<?php echo (strtolower($this->module_name) == 'recipe' && strtolower($this->uri->segment(3)) == 'main-dish' ) ? 'menu-active' : '' ?>">
                    <a href="<?php echo base_url("recipe/category/main-dish") ?>">
                        Main Dish
                    </a>
                </li>
                <li class="<?php echo (strtolower($this->module_name) == 'recipe' && strtolower($this->uri->segment(3)) == 'dessert' ) ? 'menu-active' : '' ?>">
                    <a href="<?php echo base_url("recipe/category/dessert") ?>">
                        Dessert
                    </a>
                </li>
            </ul>
        </nav>
        <!-- #nav-menu-container -->
    </div>
</header>
<!-- #header -->